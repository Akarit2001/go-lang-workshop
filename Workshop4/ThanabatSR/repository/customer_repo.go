package repository

import (
	"github.com/jinzhu/gorm"
)

type CustomerRepositoryDB struct {
	db *gorm.DB
}

func NewCustomerRepository(db *gorm.DB) CustomerRepository {
	return &CustomerRepositoryDB{db: db}
}

func (cr *CustomerRepositoryDB) GetAll() ([]Customer, error) {
	var customers []Customer
	err := cr.db.Find(&customers).Error
	return customers, err
}

func (cr *CustomerRepositoryDB) InsertCustomer(c Customer) (*Customer, error) {
	err := cr.db.Create(&c).Error
	if err != nil {
		return nil, err
	}
	return &c, nil
}
func (cr *CustomerRepositoryDB) GetById(id int) (*Customer, error) {
    customer := &Customer{}
    err := cr.db.Where("customer_id = ?", id).First(customer).Error
    if err != nil {
        return nil, err
    }
    return customer, nil
}

func (cr *CustomerRepositoryDB) DeleteCustomer(id int) error {
    err := cr.db.Delete(&Customer{CustomerID: id}).Error
    if err != nil {
        return err
    }
    return nil
}
func (cr *CustomerRepositoryDB) UpdateCustomer(id int, c Customer) (*Customer, error) {
    existingCustomer, err := cr.GetById(id)
    if err != nil {
        return nil, err
    }
    err = cr.db.Model(existingCustomer).Updates(c).Error
    if err != nil {
        return nil, err
    }
    return existingCustomer, nil
}
