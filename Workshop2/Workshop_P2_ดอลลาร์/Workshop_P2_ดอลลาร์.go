package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	_ "github.com/lib/pq"
)

const customerPath = "customers"

var CustomerList []Customer

var DB *sql.DB

const basePath = "/api"

type Customer struct {
	ID          int       `json:"id"`
	Name        string    `json:"name"`
	PhoneNumber string    `json:"phone_number"`
	DateCreate  time.Time `json:"date_create"`
}

func getCustomer(customerID int) (*Customer, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	row := DB.QueryRowContext(ctx, `SELECT
        id,
        name,
        phone_number,
        date_create
        FROM customer
        WHERE id = $1`, customerID)

	customer := &Customer{}
	err := row.Scan(
		&customer.ID,
		&customer.Name,
		&customer.PhoneNumber,
		&customer.DateCreate,
	)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		log.Println(err)
		return nil, err
	}
	return customer, nil
}
func removeCustomer(customerID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err := DB.ExecContext(ctx, `DELETE FROM customer WHERE id = $1`, customerID)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}
func getCustomerList() ([]Customer, error) {
	ctx, cancle := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancle()
	result, err := DB.QueryContext(ctx, `SELECT
	id,
	name,
	phone_number,
	date_create
	FROM customer`)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	defer result.Close()
	customers := make([]Customer, 0)
	for result.Next() {
		var customer Customer
		result.Scan(&customer.ID,
			&customer.Name,
			&customer.PhoneNumber,
			&customer.DateCreate)
		customers = append(customers, customer)
	}
	return customers, nil
}
func insertCustomer(customer Customer) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err := DB.ExecContext(ctx, `INSERT INTO customer
	(id,
		name,
		phone_number,
		date_create
	) VALUES ($1,$2,$3,$4)`,
		customer.ID,
		customer.Name,
		customer.PhoneNumber,
		time.Now())
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}

	var insertID int
	row := DB.QueryRowContext(ctx, "SELECT MAX(id) FROM customer")
	err = row.Scan(&insertID)
	if err != nil {
		log.Print(err.Error())
		return 0, err
	}
	return insertID, nil
}
func handlerCustomer(w http.ResponseWriter, r *http.Request) {
	urlPathSegment := strings.Split(r.URL.Path, fmt.Sprintf("%s/", customerPath))
	if len(urlPathSegment[1:]) > 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	customerID, err := strconv.Atoi(urlPathSegment[len(urlPathSegment)-1])
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	switch r.Method {
	case http.MethodGet:
		customer, err := getCustomer(customerID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if customer == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		j, err := json.Marshal(customer)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodDelete:
		err := removeCustomer(customerID)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func handlerCustomers(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		customerList, err := getCustomerList()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		j, err := json.Marshal(customerList)
		if err != nil {
			log.Fatal(err)
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodPost:
		var customer Customer
		err := json.NewDecoder(r.Body).Decode(&customer)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		customerID, err := insertCustomer(customer)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"id":%d}`, customerID)))
	case http.MethodOptions:
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func corsMiddleware(handler http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token")
		handler.ServeHTTP(w, r)
	}
}

func setupRoutes(apiBasePath string) {
	customerHandler := http.HandlerFunc(handlerCustomer)
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, customerPath), corsMiddleware(customerHandler))
	customersHandler := http.HandlerFunc(handlerCustomers)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, customerPath), corsMiddleware(customersHandler))
}

func setupDB() {
	const (
		host     = "localhost"
		port     = 5432
		user     = "postgres"
		password = "1234"
		dbname   = "postgres"
	)
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	var err error
	DB, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(DB)
	DB.SetConnMaxIdleTime(time.Minute * 3)
	DB.SetMaxOpenConns(10)
	DB.SetMaxIdleConns(10)
}

func main() {
	setupDB()
	setupRoutes(basePath)
	log.Fatal(http.ListenAndServe(":5000", nil))
}
