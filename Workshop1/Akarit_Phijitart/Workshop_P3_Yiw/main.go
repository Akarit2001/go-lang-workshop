package main

import "fmt"

func main() {

	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()

	var n1, n2 int

	fmt.Print("Enter first number: ")
	fmt.Scanln(&n1)
	fmt.Print("Enter second number: ")
	fmt.Scanln(&n2)
	if n2 == 0 {
		panic("Second number should not be 0!")
	}
	fmt.Printf("Addition, %d\n", n1+n2)
	fmt.Printf("Subtraction, %d\n", n1-n2)
	fmt.Printf("Multiplication, %d\n", n1*n2)
	fmt.Printf("Division, %d\n", n1/n2)

}
