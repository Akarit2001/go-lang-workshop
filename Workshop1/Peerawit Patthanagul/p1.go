package main

import "fmt"

type Product struct {
	ProductName string
	Price int
	Weight float64
}
func main() {
	ProductList := []Product{}
	Product1 :=Product{
		ProductName: "iPhone 13",
		Price: 26900,
		Weight: 0.21,
	} 
	ProductList = append(ProductList, Product1)
	fmt.Println("Product Name:",Product1.ProductName)
	fmt.Println("Price:",Product1.Price)
	fmt.Println("Weight:",Product1.Weight)
}