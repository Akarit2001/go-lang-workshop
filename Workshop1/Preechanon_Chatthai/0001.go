package main

import "fmt"

var numbers = []float64{1.5, 2.0, 3.5, 4.0, 5.5, 6.0, 7.5}

func calculateAverage(numbers []float64) {
	sum := 0.0
	for _, number := range numbers {
		sum += number
	}
	average := sum / float64(len(numbers))
	fmt.Println(average)
}

func main() {
	calculateAverage(numbers)
}
