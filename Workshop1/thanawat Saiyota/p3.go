package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func getInput(promt string) float64 {
	fmt.Printf("%v", promt) //%v is a data type
	input, _ := reader.ReadString('\n')
	value, err := strconv.ParseFloat(strings.TrimSpace(input), 64)
	if err != nil {
		message, _ := fmt.Scanf("%v must numver only", promt)
		panic(message)
	}
	return value
}

func add(value1, value2 float64) float64 {
	return value1 + value2
}

func subtract(value1, value2 float64) float64 {
	return value1 - value2
}

func multiply(value1, value2 float64) float64 {
	return value1 * value2
}

func divide(value1, value2 float64) float64 {
	return value1 / value2
}

func getOperator() string {
	fmt.Print("operator is (+ - * /): ")
	op, _ := reader.ReadString('\n')
	return strings.TrimSpace(op)
}

func print(value1, value2 float64) {
	fmt.Printf("Adding result: %g \n", add(value1, value2))
	fmt.Printf("Subtracting result: %g \n", subtract(value1, value2))
	fmt.Printf("Multiplication: %g \n", multiply(value1, value2))

	if value2 == 0 {
		fmt.Println("Division by zero!")
	} else {
		fmt.Printf("Division: %g \n", divide(value1, value2))
	}
}

func main() {
	value1 := getInput("Enter first number : ")
	value2 := getInput("Enter second number :")
	print(value1, value2)

}
