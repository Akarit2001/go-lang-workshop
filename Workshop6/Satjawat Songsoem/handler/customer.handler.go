package handler

import (
	"fmt"
	"gofiber/service"

	//"github.com/afex/hystrix-go/hystrix"
	"github.com/gofiber/fiber/v2"
)

type customerHandler struct {
	custSrv service.CustomerService
}

func NewCustomerHandler(custSrv service.CustomerService) CustomerHandler {
	return customerHandler{custSrv}
}

func (h customerHandler) GetCustomers(c *fiber.Ctx) error {
	//output := make(chan string, 1)
	//hystrix.Go("customer", func() error {
		customers, err := h.custSrv.GetCustomers()
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
		}
		return c.JSON(customers)
	}/*, func(err error) error {
		fmt.Println(err)

		return err
	})
	out := <-output
	return c.SendString(out)
}*/

/*
func (h customerHandler) RemoveCustomer(c *fiber.Ctx) error {
	customerID, err := strconv.Atoi(c.Params("customerID"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	err = h.custSrv.RemoveCustomer(customerID)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	return c.SendString("Removed Customer Complete")
}

func (h customerHandler) InsertCustomer(c *fiber.Ctx) error {
	var customer service.CustomerResponse
	err := c.BodyParser(&customer)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}

	insertedID, err := h.custSrv.InsertCustomer(customer)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	_ = insertedID

	return c.SendString("Insert Customer Complete")
}

func (h customerHandler) UpdateCustomer(c *fiber.Ctx) error {
	var customer service.CustomerResponse
	err := c.BodyParser(&customer)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	updateID, err := h.custSrv.UpdateCustomer(customer)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	_ = updateID

	return c.SendString("Update Customer Complete")
}

func (h customerHandler) GetCustomer(c *fiber.Ctx) error {
	customerID, err := strconv.Atoi(c.Params("customerID"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString(fmt.Sprintf("Error: %v", err))
	}
	customer, err := h.custSrv.GetCustomer(customerID)
	if err != nil {
		appErr, ok := err.(errs.AppError)
		if ok {
			return c.Status(appErr.Code).SendString(appErr.Message)
		}
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	return c.JSON(customer)
}
*/
